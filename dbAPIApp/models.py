from django.db import models
from django.contrib.auth.models import User as defaultUser
# Create your models here.



class URIsModel(models.Model):
    uri = models.CharField(max_length=36)
    related_user_account = models.ForeignKey(defaultUser, on_delete=models.CASCADE)

    def __str__(self):
        return 'URI: ' + str(self.related_user_account)

    class Meta:
        verbose_name = 'URI'
        verbose_name_plural = 'URIs'
        


class BooksModel(models.Model):
    code = models.IntegerField() # id field is already reserved for pk by the base model, so we define ours
    title = models.CharField(max_length=120)
    author = models.CharField(max_length=120)
    related_user_URI = models.ForeignKey(URIsModel, on_delete=models.CASCADE)
    
    class Meta:
        verbose_name = 'Book'
        # A given user might mistakenly send us two similar ids for two different books. We want to prevent that.
        # Moreover, different users might send two identical book information, which is a valid scenario, and we want to allow that.
        # Binding the URI to each book code solves the problem.
        unique_together = (('related_user_URI', 'code'),)


class MoviesModel(models.Model):
    code = models.IntegerField()
    title = models.CharField(max_length=120)
    director = models.CharField(max_length=120)
    related_user_URI = models.ForeignKey(URIsModel, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Movie'
        unique_together = (('related_user_URI', 'code'),)


class ProfileModel(models.Model):
    name = models.CharField(max_length=120)
    related_user_URI = models.ForeignKey(URIsModel, on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Profile'