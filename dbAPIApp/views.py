from django.shortcuts import render
from django.contrib.auth.models import User
# Create your views here.

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
import uuid
from .models import URIsModel, BooksModel, MoviesModel, ProfileModel
import json, jsonschema

class HelloView(APIView):
    permission_classes = (IsAuthenticated,)
    
    def get(self, request):
        content = {'message': 'Hello, World!'}
        return Response(content)

class CreateURI(APIView):
    permission_classes = (IsAuthenticated,)

    # Schema for validation of json data sent by user
    inferredSchema = {
            "$schema": "http://json-schema.org/draft-04/schema#",
            "type": "object",
            "properties": {
                "books": {
                "type": "array",
                "items": [
                    {
                    "type": "object",
                    "properties": {
                        "id": {
                        "type": "integer"
                        },
                        "title": {
                        "type": "string"
                        },
                        "author": {
                        "type": "string"
                        }
                    },
                    "required": [
                        "id",
                        "title",
                        "author"
                    ]
                    }
                ]
                },
                "movies": {
                "type": "array",
                "items": [
                    {
                    "type": "object",
                    "properties": {
                        "id": {
                        "type": "integer"
                        },
                        "title": {
                        "type": "string"
                        },
                        "director": {
                        "type": "string"
                        }
                    },
                    "required": [
                        "id",
                        "title",
                        "director"
                    ]
                    }
                ]
                },
                "profile": {
                "type": "object",
                "properties": {
                    "name": {
                    "type": "string"
                    }
                },
                "required": [
                    "name"
                ]
                }
            }
            }
    
    def post(self, request):

        # Validating received JSON before continuing
        try:
            # Read in the JSON document
            validated_json = json.loads(request.body)
            validated_json = validated_json['structure']
            # And validate the result
            jsonschema.validate(validated_json, self.inferredSchema)
        except jsonschema.exceptions.ValidationError as e:
            return Response("JSON data format is valid, but the required data structure is not met:", e)
        except json.decoder.JSONDecodeError as e:
            return Response("JSON data format is invalid:", e)

        id_of_current_user = request.auth.payload['user_id']
        generated_uri = uuid.uuid4() # Generate URI
        
        # Authenticated users providing valid tokens are allowed to create as many URIs as they wish.
        # Here we store the URI and the corresponding user immediately after generation.
        # Users can call /api/MyURIs/ to view and manage their existing URIs.
        temp_URI_instance = URIsModel()
        temp_URI_instance.uri = str(generated_uri)
        temp_URI_instance.related_user_account = User.objects.get(id=id_of_current_user)
        temp_URI_instance.save()

        # Iterate and store the data into the database
        for key in validated_json:
            if key == 'books':
                for book_details in validated_json['books']:
                    temp = BooksModel(
                        title=book_details['title'],
                        code=book_details['id'],
                        author=book_details['author'],
                        related_user_URI=temp_URI_instance
                    )
                    temp.save()
            elif key == 'movies':
                for movie_details in validated_json['movies']:
                    temp = MoviesModel(
                        title=movie_details['title'],
                        code=movie_details['id'],
                        director=movie_details['director'],
                        related_user_URI=temp_URI_instance
                    )
                    temp.save()
            elif key == 'profile':
                temp = ProfileModel(
                    name=validated_json['profile']['name'],
                    related_user_URI=temp_URI_instance
                )
                temp.save()


        return Response({'URI': str(generated_uri)})


class ListURIs(APIView):
    
    ''' This view allows users to view their existing URIs'''
    
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        id_of_current_user = request.auth.payload['user_id']
        user_object = User.objects.get(id=id_of_current_user)
        results = URIsModel.objects.filter(related_user_account=user_object)
        temp_list = []
        for item in results:
            temp_list.append(item.uri)

        return Response(temp_list)

class ManageData(APIView):
    permission_classes = (IsAuthenticated,)


    def find_resource(self, request, uri, item_type, item_number=''):
        """This function finds the addressed resource and returns it for processing by GET/POST/DELETE/PUT methods"""

        # Ensure the provided URI belongs to the user providing it. This prevents an authorized user from accessing URIs belonging to other users. Rare scenario but possible.
        result = URIsModel.objects.filter(uri=uri)
        if result.count() == 0:
            return Response("Not allowed")

        if item_number:
            if item_type == 'books':
                # Applying .values on filter results causes the returned query to be lost. So we return both for later use.
                result = BooksModel.objects.filter(code=item_number, related_user_URI=URIsModel.objects.get(uri=uri))
                result_with_values = result.values('code', 'title', 'author')
                return [result, result_with_values]

            elif item_type == 'movies':
                result = MoviesModel.objects.filter(code=item_number, related_user_URI=URIsModel.objects.get(uri=uri))
                result_with_values = result.values('code', 'title', 'director')
                return [result, result_with_values]

        else:
            if item_type == 'books':
                result = BooksModel.objects.filter(related_user_URI=URIsModel.objects.get(uri=uri))
                result_with_values = result.values('code', 'title', 'author')
                return [result, result_with_values]

            elif item_type == 'movies':
                result = MoviesModel.objects.filter(related_user_URI=URIsModel.objects.get(uri=uri))
                result_with_values = result.values('code', 'title', 'director')
                return [result, result_with_values]


    def get(self, request, uri, item_type, item_number=''):

        _, result_with_values = self.find_resource(request, uri, item_type, item_number)
        return Response(list(result_with_values))


    def delete(self, request, uri, item_type, item_number=''):

        result, _ = self.find_resource(request, uri, item_type, item_number)
        result.delete()
        return Response("Delete Successful")


    def put(self, request, uri, item_type, item_number=''):
        """ This method updates a single object in the database. A valid JSON structure is as follows:
            {
                "code": 1,
                "title": "some title",
                "director": "some director name"
                }
            """
        result, _ = self.find_resource(request, uri, item_type, item_number)
        if result.count() > 1:
            return Response('Multiple queries found, but the PUT method can only be applied to a single object. Provide a key at the end of the URL.')
        elif result.count() == 0:
            return Response('Object not found.')
        else:
            # Data here must be sanitized before update, but no time to actually implement it.
            request_dict = request.data.dict()
            request_dict['code'] = request_dict['id']
            del request_dict['id']
            
            result.update(**request_dict)
            return Response("Update Successful")
    
    def post(self, request, uri, item_type, item_number=''):
        ''' The following format (which is a list) must be observed when adding new items:
        sample_data = [{
                "id":1,
                "title": "some title",
                "author": "some author"
                }]
        '''
        # Much of the code below is similar to what we have for /api/create-uri/. Refactoring can be done.
        schema = {
        "$schema": "http://json-schema.org/draft-04/schema#",
        "type": "array",
        "items": [
            {
            "type": "object",
            "properties": {
                "id": {
                "type": "integer"
                },
                "title": {
                "type": "string"
                },
                "author": {
                "type": "string"
                }
            },
            "required": [
                "id",
                "title",
                "author"
            ]
            }
        ]
        }

        # Validating received JSON before continuing
        try:
            # Read in the JSON document
            validated_json = json.loads(request.body)
            # And validate the result
            jsonschema.validate(validated_json, schema)
        except jsonschema.exceptions.ValidationError as e:
            return Response("JSON data format is valid, but the required data structure is not met:", e)
        except json.decoder.JSONDecodeError as e:
            return Response("JSON data format is invalid:", e)


        temp_URI_instance = URIsModel.objects.get(uri=uri)

        try:
            for item in validated_json:
                if item_type == 'books':
                    temp = BooksModel(
                        title=item['title'],
                        code=item['id'],
                        author=item['author'],
                        related_user_URI=temp_URI_instance
                    )
                    temp.save()
                elif item_type == 'movies':
                    temp = MoviesModel(
                        title=item['title'],
                        code=item['id'],
                        director=item['director'],
                        related_user_URI=temp_URI_instance
                    )
                    temp.save()
        except:
            return Response("Error adding new item(s)")
        return Response("New item added successfully")
            


        
