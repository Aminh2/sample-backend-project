from django.apps import AppConfig


class DbapiappConfig(AppConfig):
    name = 'dbAPIApp'
