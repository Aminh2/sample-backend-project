from django.contrib import admin

from .models import URIsModel, BooksModel, MoviesModel, ProfileModel
# Register your models here.

class URIAdmin(admin.ModelAdmin):
    list_display = ('related_user_account', 'uri')

admin.site.register(BooksModel)
admin.site.register(MoviesModel)
admin.site.register(ProfileModel)
admin.site.register(URIsModel, URIAdmin)